from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'I would like to congratulate you for somehow landing on this page, although I have to say that this page has little to no meaning at all. Nonetheless, I hope you will have an enjoyable stay here, on this mostly insignificant page, made with countless of tears and dozens of attempts to free fall off the fifth floor.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)